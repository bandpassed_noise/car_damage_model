import os


def list_files(path: str):
    # result = next(os.walk(path), (None, None, []))[2]  # [] if no file
    result = sorted(list(os.listdir(path)))  # 'same' as above
    return result


def filter_to_jpgs(file_list: list):
    jpeg_list = ['JPEG', 'jpeg', 'jpg']  # make sure only .jpegs are there
    result = [i for i in file_list if any(i for j in jpeg_list if str(j) in i)]
    return result
