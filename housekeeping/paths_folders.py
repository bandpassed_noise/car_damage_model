import os


# change wd if current wd is not preferred wd // change to general data
def change_wd(destination: str):
    in_data = 'data' in os.getcwd()
    in_preferred = destination in os.getcwd()
    if not in_data:
        os.chdir(os.path.join(os.getcwd(), 'data'))  # if in project folder, go to 'data'
    if not in_preferred and destination != 'data':
        os.chdir(os.path.join(os.getcwd(), destination))  # if already in 'data' folder, go to preferred folder

    in_data = 'data' in os.getcwd()[-4:]
    if destination == 'data' and in_data is False:
        os.chdir(os.path.normpath(os.getcwd() + os.sep + os.pardir))  # change to 'data' if argument is 'data'


# create a folder if it doesn't exist
def maybe_make_dir(directory: str):
    if not os.path.exists(os.path.join(os.getcwd(), directory)):
        os.makedirs(os.path.join(os.getcwd(), directory))


# for easier navigation to a given end folder
def path_getter(destination: str):
    target = os.path.join(os.getcwd(), destination)
    return target
