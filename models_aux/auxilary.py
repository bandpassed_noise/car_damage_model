import os
import re
import datetime
import cv2
import boto3
import tempfile
import zipfile
import logging
import shutil  # zip save
import pathlib
from keras.models import load_model, Model
from dotenv import load_dotenv

load_dotenv()

AWS_ACCESS_KEY = os.getenv('AWS_ACCESS_KEY')
AWS_SECRET_KEY = os.getenv('AWS_SECRET_KEY')
BUCKET_NAME = os.getenv('BUCKET_NAME')


# model checkpoint filepath (will be used when training a model with optimized hyperparameters)
def checkpoint_path(model_path: str, model_name: str):
    model_w_str = '{epoch:02d}-{val_accuracy:.3f}-{accuracy:.3f}.hdf5'
    result = os.path.join(model_path,
                          f"{model_name}_" + str(datetime.datetime.now()).split(' ')[0] + "__" + model_w_str)
    return result


def get_s3():
    return boto3.client('s3',
                        aws_access_key_id=AWS_ACCESS_KEY,
                        aws_secret_access_key=AWS_SECRET_KEY)


def zip_model(keras_model, mdl_path: str, mdl_name: str):
    keras_model.save(os.path.join(mdl_path, mdl_name))
    # Zip it up first
    shutil.make_archive(os.path.join(mdl_path, mdl_name), 'zip', os.path.join(mdl_path, mdl_name))


def s3_upload_file(s3_folder, local_file_path):
    s3_client = get_s3()
    s3_path = os.path.join(s3_folder, os.path.basename(local_file_path))
    with open(local_file_path, mode='rb') as data:
        s3_client.upload_fileobj(data, BUCKET_NAME, s3_path)
    logging.info(f'Saved zipped model at path s3://{BUCKET_NAME}/{os.path.basename(local_file_path)}.zip')


def s3_get_zip_model(model_name: str) -> Model:
    with tempfile.TemporaryDirectory() as tempdir:
        s3 = get_s3()
        mdl_path = f'{tempdir}/{model_name}'
        # Fetch and save the zip file to the temporary directory
        s3.download_file(BUCKET_NAME, 'models/' + model_name, f'{mdl_path}.zip')
        # Extract the model zip file within the temporary directory
        with zipfile.ZipFile(f'{mdl_path}.zip') as zip_ref:
            zip_ref.extractall(mdl_path)
        # Load the keras model from the temporary directory
        return load_model(mdl_path)


def s3_get_h5_model(model_name: str) -> Model:
    with tempfile.TemporaryDirectory() as tempdir:
        s3 = get_s3()
        mdl_path = f'{tempdir}/{model_name}'
        # Fetch and save the h5 file to the temporary directory
        s3.download_file(BUCKET_NAME, 'models/' + model_name, f'{mdl_path}')
        # Load the keras model from the temporary directory
        return load_model(mdl_path)


# function - extract val accuracy and train accuracy from a given saved model name
def model_gatekeeper(model_folder: str, model_name: str, val_acc_threshold: int, train_acc_threshold: int):
    metrics = model_name.partition('__')[2]  # faster than split
    metrics = metrics.partition('.')[2].partition('.')
    val_accuracy = int(re.findall(r'\d+', metrics[0])[0])
    train_accuracy = int(re.findall(r'\d+', metrics[2])[0])
    val_bool = val_accuracy >= val_acc_threshold
    train_bool = train_accuracy >= train_acc_threshold
    green_light = (val_bool + train_bool) == 2
    if not green_light:
        os.remove(os.path.join(os.getcwd(), model_folder, model_name))


# function for cropping each detection and saving as new image
def crop_yoloed(img_path: str, results_df, write_path: str):
    image = cv2.imread(img_path)
    base_name = pathlib.PurePath(img_path).name.split('.')[0]  # get image name

    num_objects = results_df.shape[0]
    boxes = results_df.iloc[:, 0:4]
    scores = results_df.iloc[:, 4]
    class_names = results_df.iloc[:, 6]
    # create dictionary to hold count of objects for image name
    for i in range(num_objects):
        # get count of class for part of image name
        class_name = class_names[i]
        # get box coordinates
        xmin, ymin, xmax, ymax = boxes.iloc[i]
        # crop detection from image (take an additional 5 pixels around all edges)
        cropped_img = image[int(ymin) - 0:int(ymax) + 0, int(xmin) - 0:int(xmax) + 0]  # 0 pixels added to the frame
        if cropped_img.shape[0] > 280 and cropped_img.shape[1] > 280:  # save only if the image is somewhat large
            # construct image name and join it to path for saving crop properly
            img_name = class_name + '_' + base_name + '_' + str(round(scores[i], 2)) + '.jpg'
            final_path = os.path.join(write_path, img_name)
            # save image
            cv2.imwrite(final_path, cropped_img)

