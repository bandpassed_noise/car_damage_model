import numpy as np
import os
import cv2  # pip install opencv-python

from sklearn.preprocessing import LabelBinarizer

from housekeeping.paths_folders import path_getter, maybe_make_dir, change_wd
from housekeeping.images import list_files, filter_to_jpgs

from keras import optimizers
from keras.models import Sequential
from keras.layers import Dense, Flatten, MaxPooling2D, Dropout, Conv2D

from tensorflow.keras.utils import img_to_array

from keras_tuner.tuners import BayesianOptimization  # pip install keras-tuner

# import warnings
# warnings.filterwarnings("ignore")
model_name = 'model_tuner_base_aug'
project_name = 'tuner_kaggle_toy'
np.set_printoptions(suppress=True)  # scientific notation off


def build_model(hp):
    model_build = Sequential()

    hp_units_1 = hp.Int('units_1', min_value=16, max_value=32, step=16)
    hp_units_2 = hp.Int('units_2', min_value=32, max_value=64, step=32)
    hp_units_3 = hp.Int('units_3', min_value=64, max_value=128, step=64)
    hp_dropout = hp.Choice('rate', values=[0.25, 0.5])
    hp_learning_rate = hp.Choice('learning_rate', values=[0.002, 0.0015])

    model_build.add(Conv2D(hp_units_1, (3, 3), activation='relu', input_shape=(250, 250, 3)))
    model_build.add(Conv2D(hp_units_1, (3, 3), activation='relu', input_shape=(250, 250, 3)))
    model_build.add(MaxPooling2D((2, 2)))
    model_build.add(Conv2D(hp_units_2, (3, 3), activation='relu'))
    model_build.add(MaxPooling2D((2, 2)))
    model_build.add(Conv2D(128, (3, 3), activation='relu'))
    model_build.add(MaxPooling2D((2, 2)))
    model_build.add(Conv2D(128, (3, 3), activation='relu'))
    model_build.add(MaxPooling2D((2, 2)))
    model_build.add(Flatten())
    model_build.add(Dropout(rate=hp_dropout))
    model_build.add(Dense(hp_units_3, activation='relu'))
    model_build.add(Dense(1, activation='sigmoid'))

    adam_opt = optimizers.Adam(learning_rate=hp_learning_rate)

    model_build.compile(loss='binary_crossentropy', optimizer=adam_opt, metrics=['accuracy'])

    return model_build


# paths
change_wd('data')
os.getcwd()

training_path = path_getter(os.path.join('stored_images', 'training_aug'))
validation_path = path_getter(os.path.join('stored_images', 'validation_cropped'))
predict_path = path_getter(os.path.join('stored_images', 'predict_cropped'))

# model folder
model_path = path_getter(model_name)

tuner = BayesianOptimization(
    build_model,
    objective='val_accuracy',
    max_trials=15,
    directory=model_path,
    project_name=project_name)

# laod n best models and recreate the best one
model = tuner.get_best_models(num_models=1)
model = model[0]

###
# data
###

# to populate later on
data = []
labels = []

# append data and its labels
damaged_paths = list_files(os.path.join(training_path, "00-damage"))  # get images' paths - damaged
damaged_paths = filter_to_jpgs(damaged_paths)
for pic in damaged_paths:
    image = cv2.imread(os.path.join(training_path, "00-damage", pic))
    image = img_to_array(image)
    data.append(image)
    labels.append(0)

undamaged_paths = list_files(os.path.join(training_path, "01-whole"))  # get images' paths - undamaged
undamaged_paths = filter_to_jpgs(undamaged_paths)
for pic in undamaged_paths:
    image = cv2.imread(os.path.join(training_path, "01-whole", pic))
    image = img_to_array(image)
    data.append(image)
    labels.append(1)

# do the same for validation data
data_val = []
labels_val = []

# append data and its labels
damaged_paths_val = list_files(os.path.join(validation_path, "00-damage"))  # get images' paths - damaged
damaged_paths_val = filter_to_jpgs(damaged_paths_val)
for pic in damaged_paths_val:
    image = cv2.imread(os.path.join(validation_path, "00-damage", pic))
    image = img_to_array(image)
    data_val.append(image)
    labels_val.append(0)
data = np.array(data, dtype="float32")

undamaged_paths_val = list_files(os.path.join(validation_path, "01-whole"))  # get paths - undamaged
undamaged_paths_val = filter_to_jpgs(undamaged_paths_val)
for pic in undamaged_paths_val:
    image = cv2.imread(os.path.join(validation_path, "01-whole", pic))
    image = img_to_array(image)
    data_val.append(image)
    labels_val.append(1)
data_val = np.array(data_val, dtype="float32")

# transform labels
mlb = LabelBinarizer()

labels = np.array(labels)
labels = mlb.fit_transform(labels)

labels_val = np.array(labels_val)
labels_val = mlb.fit_transform(labels_val)

# reassign data
data = data / 255.0
data_val = data_val / 255.0  # input has to be between 0 and 1
(xtrain, xtest, ytrain, ytest) = (data, data_val, labels, labels_val)

# check train / validation data dimensions
print(xtrain.shape, xtest.shape)

# predict data
# to populate later on
data_predict = []

# append data
predict_img_paths = list_files(predict_path)  # get images' paths - predict
predict_img_paths = filter_to_jpgs(predict_img_paths)
for pic in predict_img_paths:
    image = cv2.imread(os.path.join(predict_path, pic))
    image = img_to_array(image)
    data_predict.append(image)

data_predict = np.array(data_predict)
data_predict = data_predict / 255.0  # input has to be between 0 and 1

###
# eval & predict
###

eval_result = model.evaluate(xtest, ytest)
print("[test loss, test accuracy]:", eval_result)

print(model.predict(data_predict).shape)
print(predict_img_paths)
print(model.predict(data_predict))
