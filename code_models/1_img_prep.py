import os
import cv2  # pip install opencv-python
import numpy as np
from housekeeping.paths_folders import path_getter, maybe_make_dir, change_wd
from housekeeping.images import list_files, filter_to_jpgs
from keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.utils import load_img, img_to_array, save_img


# paths
change_wd('stored_images')
os.getcwd()

training_path = path_getter('training')
validation_path = path_getter('validation')
predict_path = path_getter('predict')

# create folders for augmented data
maybe_make_dir('training_aug')

aug_path = path_getter('training_aug')
maybe_make_dir(os.path.join(aug_path, '00-damage'))
maybe_make_dir(os.path.join(aug_path, '01-whole'))

# create folders for processed non-augmented images
maybe_make_dir('training_cropped')
maybe_make_dir('validation_cropped')
maybe_make_dir('predict_cropped')

train_cropped_path = path_getter('training_cropped')
maybe_make_dir(os.path.join(train_cropped_path, '00-damage'))
maybe_make_dir(os.path.join(train_cropped_path, '01-whole'))

validation_cropped_path = path_getter('validation_cropped')
maybe_make_dir(os.path.join(validation_cropped_path, '00-damage'))
maybe_make_dir(os.path.join(validation_cropped_path, '01-whole'))

predict_cropped_large_path = path_getter('predict_cropped')

# list images
train_damaged_list = list_files(os.path.join(training_path, '00-damage'))
train_not_damaged_list = list_files(os.path.join(training_path, '01-whole'))

val_damaged_list = list_files(os.path.join(validation_path, '00-damage'))
val_not_damaged_list = list_files(os.path.join(validation_path, '01-whole'))

predict_damaged_list = list_files(predict_path)

# make sure only .jpegs are there
train_damaged_list = filter_to_jpgs(train_damaged_list)
train_not_damaged_list = filter_to_jpgs(train_not_damaged_list)

val_damaged_list = filter_to_jpgs(val_damaged_list)
val_not_damaged_list = filter_to_jpgs(val_not_damaged_list)

predict_damaged_list = filter_to_jpgs(predict_damaged_list)

# Data Augmentation 1
# enlarging the dataset twice using random rotation between -20 and 20 degrees and horizontal flip transformations

augmentation = ImageDataGenerator(rotation_range=20, horizontal_flip=True)
size = 250  # images to be cropped to...

# run through image folders and augment data
for pic in train_damaged_list:
    sample_img = load_img(os.path.join(training_path, '00-damage', pic))
    sample_img_array = img_to_array(sample_img)
    sample_img_4d = sample_img_array.reshape((1,) + sample_img_array.shape)

    # test augmentation
    aug_sample = augmentation.flow(sample_img_4d, batch_size=1, save_format='jpg')

    # extract array from data generator obj
    aug_result = np.concatenate([aug_sample.next()[0] for i in range(aug_sample.__len__())])
    aug_result = cv2.resize(aug_result, (size, size))  # resize
    save_img(os.path.join(aug_path, '00-damage', f'aug_train_damaged_{pic}'), aug_result)

    # now crop the initial image and also save it
    init_img = cv2.resize(sample_img_array, (size, size))  # resize
    save_img(os.path.join(aug_path, '00-damage', f'init_train_damaged_{pic}'), init_img)
    # also save to a separate folder with cropped only
    save_img(os.path.join(train_cropped_path, '00-damage', f'init_train_damaged_{pic}'), init_img)

# now the undamaged cars
for pic in train_not_damaged_list:
    sample_img = load_img(os.path.join(training_path, '01-whole', pic))
    sample_img_array = img_to_array(sample_img)
    sample_img_4d = sample_img_array.reshape((1,) + sample_img_array.shape)

    # test augmentation
    aug_sample = augmentation.flow(sample_img_4d, batch_size=1, save_format='jpg')

    # extract array from data generator obj
    aug_result = np.concatenate([aug_sample.next()[0] for i in range(aug_sample.__len__())])
    aug_result = cv2.resize(aug_result, (size, size))  # resize
    save_img(os.path.join(aug_path, '01-whole', f'aug_train_undamaged_{pic}'), aug_result)

    # now crop the initial image and also save it
    init_img = cv2.resize(sample_img_array, (size, size))  # resize
    save_img(os.path.join(aug_path, '01-whole', f'init_train_undamaged_{pic}'), init_img)
    # also save to a separate folder with cropped only
    save_img(os.path.join(train_cropped_path, '01-whole', f'init_train_undamaged_{pic}'), init_img)

###
# now validation dataset
###

for pic in val_damaged_list:
    sample_img = load_img(os.path.join(validation_path, '00-damage', pic))
    sample_img_array = img_to_array(sample_img)

    # now crop the initial image and save it
    init_img = cv2.resize(sample_img_array, (size, size))  # resize
    save_img(os.path.join(validation_cropped_path, '00-damage', f'init_validation_damaged_{pic}'), init_img)

# undamaged cars
for pic in val_not_damaged_list:
    sample_img = load_img(os.path.join(validation_path, '01-whole', pic))
    sample_img_array = img_to_array(sample_img)

    # now crop the initial image and save it
    init_img = cv2.resize(sample_img_array, (size, size))  # resize
    save_img(os.path.join(validation_cropped_path, '01-whole', f'init_validation_damaged_{pic}'), init_img)

# predict set
for pic in predict_damaged_list:
    sample_img = load_img(os.path.join(predict_path, pic))
    sample_img_array = img_to_array(sample_img)

    # now crop the initial image and save it
    init_img = cv2.resize(sample_img_array, (size, size))  # resize
    save_img(os.path.join(predict_cropped_large_path, f'init_predict_damaged_{pic}'), init_img)
