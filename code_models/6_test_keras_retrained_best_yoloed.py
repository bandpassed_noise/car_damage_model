import numpy as np
import os
import cv2  # pip install opencv-python

from housekeeping.paths_folders import path_getter, maybe_make_dir, change_wd
from housekeeping.images import list_files, filter_to_jpgs
from models_aux.auxilary import zip_model, s3_upload_file, s3_get_zip_model, s3_get_h5_model

from keras.models import load_model

from tensorflow.keras.utils import img_to_array

# import warnings
# warnings.filterwarnings("ignore")
model_name = 'model_tuner_base_aug'
project_name = 'tuner_kaggle_toy'
np.set_printoptions(suppress=True)  # scientific notation off


# paths
change_wd('data')
os.getcwd()

predict_path = path_getter(os.path.join('stored_images', 'predict_cropped', 'yoloed_cropped'))

# model folder
model_path = path_getter(model_name)

# laod n best models and recreate the best one
model = load_model(os.path.join(model_path, model_name) + '.h5')

###
# data
###
# predict data
# to populate later on
data_predict = []

# append data
predict_img_paths = list_files(predict_path)  # get images' paths - predict
predict_img_paths = filter_to_jpgs(predict_img_paths)
for pic in predict_img_paths:
    image = cv2.imread(os.path.join(predict_path, pic))
    image = img_to_array(image)
    data_predict.append(image)

data_predict = np.array(data_predict)
data_predict = data_predict / 255.0  # input has to be between 0 and 1

###
# eval & predict
###

print(model.predict(data_predict).shape)
print(predict_img_paths)
print(model.predict(data_predict))

# test on single image
image2 = image[np.newaxis, ...]
print(model.predict(image2))
###
# save model to s3
###

# # zip and upload to s3
# zip_model(model, model_path, model_name)  # skip zipping to upload .h5 format
# s3_upload_file('models/', os.path.join(model_path, model_name + '.zip'))

s3_upload_file('models/', os.path.join(model_path, model_name + '.h5'))  # upload .h5 format

# load model from s3
# loaded_model = s3_get_zip_model('model_tuner_base_aug.zip')
loaded_model = s3_get_h5_model('model_tuner_base_aug.h5')
print(loaded_model.predict(data_predict))


