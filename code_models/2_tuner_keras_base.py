import numpy as np
import os
import cv2  # pip install opencv-python
from sklearn.preprocessing import LabelBinarizer

from housekeeping.paths_folders import path_getter, maybe_make_dir, change_wd
from housekeeping.images import list_files, filter_to_jpgs
from models_aux.auxilary import checkpoint_path

from keras import optimizers
from keras.models import Sequential, load_model
from keras.layers import Dense, Flatten, MaxPooling2D, Dropout, Conv2D

from tensorflow.keras.utils import img_to_array

from keras.callbacks import ModelCheckpoint, ReduceLROnPlateau, EarlyStopping, Callback
from keras_tuner.tuners import BayesianOptimization  # pip install keras-tuner

# import warnings
# warnings.filterwarnings("ignore")
model_name = 'model_tuner_base_aug'
project_name = 'tuner_kaggle_toy'


def build_model(hp):
    model_build = Sequential()

    hp_units_1 = hp.Int('units_1', min_value=16, max_value=32, step=16)
    hp_units_2 = hp.Int('units_2', min_value=32, max_value=64, step=32)
    hp_units_3 = hp.Int('units_3', min_value=64, max_value=128, step=64)
    hp_dropout = hp.Choice('rate', values=[0.25, 0.5])
    hp_learning_rate = hp.Choice('learning_rate', values=[0.002, 0.0015])

    model_build.add(Conv2D(hp_units_1, (3, 3), activation='relu', input_shape=(250, 250, 3)))
    model_build.add(Conv2D(hp_units_1, (3, 3), activation='relu', input_shape=(250, 250, 3)))
    model_build.add(MaxPooling2D((2, 2)))
    model_build.add(Conv2D(hp_units_2, (3, 3), activation='relu'))
    model_build.add(MaxPooling2D((2, 2)))
    model_build.add(Conv2D(128, (3, 3), activation='relu'))
    model_build.add(MaxPooling2D((2, 2)))
    model_build.add(Conv2D(128, (3, 3), activation='relu'))
    model_build.add(MaxPooling2D((2, 2)))
    model_build.add(Flatten())
    model_build.add(Dropout(rate=hp_dropout))
    model_build.add(Dense(hp_units_3, activation='relu'))
    model_build.add(Dense(1, activation='sigmoid'))

    adam_opt = optimizers.Adam(learning_rate=hp_learning_rate)

    model_build.compile(loss='binary_crossentropy', optimizer=adam_opt, metrics=['accuracy'])

    return model_build


# paths
change_wd('data')
os.getcwd()

training_path = path_getter(os.path.join('stored_images', 'training_aug'))
validation_path = path_getter(os.path.join('stored_images', 'validation_cropped'))

# model folder
model_path = path_getter(model_name)
maybe_make_dir(model_path)

tuner = BayesianOptimization(
    build_model,
    objective='val_accuracy',
    max_trials=15,
    directory=model_path,
    project_name=project_name)

# to populate later on
data = []
labels = []

# append data and its labels
damaged_paths = list_files(os.path.join(training_path, "00-damage"))  # get images' paths - damaged
damaged_paths = filter_to_jpgs(damaged_paths)
for pic in damaged_paths:
    image = cv2.imread(os.path.join(training_path, "00-damage", pic))
    image = img_to_array(image)
    data.append(image)
    labels.append(0)

undamaged_paths = list_files(os.path.join(training_path, "01-whole"))  # get images' paths - undamaged
undamaged_paths = filter_to_jpgs(undamaged_paths)
for pic in undamaged_paths:
    image = cv2.imread(os.path.join(training_path, "01-whole", pic))
    image = img_to_array(image)
    data.append(image)
    labels.append(1)

# do the same for validation data
data_val = []
labels_val = []

# append data and its labels
damaged_paths_val = list_files(os.path.join(validation_path, "00-damage"))  # get images' paths - damaged
damaged_paths_val = filter_to_jpgs(damaged_paths_val)
for pic in damaged_paths_val:
    image = cv2.imread(os.path.join(validation_path, "00-damage", pic))
    image = img_to_array(image)
    data_val.append(image)
    labels_val.append(0)
data = np.array(data, dtype="float32")

undamaged_paths_val = list_files(os.path.join(validation_path, "01-whole"))  # get paths - undamaged
undamaged_paths_val = filter_to_jpgs(undamaged_paths_val)
for pic in undamaged_paths_val:
    image = cv2.imread(os.path.join(validation_path, "01-whole", pic))
    image = img_to_array(image)
    data_val.append(image)
    labels_val.append(1)
data_val = np.array(data_val, dtype="float32")

# transform labels
mlb = LabelBinarizer()

labels = np.array(labels)
labels = mlb.fit_transform(labels)

labels_val = np.array(labels_val)
labels_val = mlb.fit_transform(labels_val)

# reassign data
data = data / 255.0
data_val = data_val / 255.0  # input has to be between 0 and 1
(xtrain, xtest, ytrain, ytest) = (data, data_val, labels, labels_val)

# check train / validation data dimensions
print(xtrain.shape, xtest.shape)

# checkpoints
lr_reduction = ReduceLROnPlateau(monitor='val_accuracy', factor=0.75, patience=5, verbose=1, min_lr=0.0005)
early_stop = EarlyStopping(monitor='val_accuracy', patience=15)

# model saving based on validation accuracy score
checkpoint = ModelCheckpoint(filepath=checkpoint_path(model_path, model_name), monitor='val_accuracy', verbose=1,
                             save_best_only=True, save_weights_only=True)

batch_n = 32
# fit the model
tuner.search(x=xtrain,
             y=ytrain,
             batch_size=batch_n,
             epochs=45,
             steps_per_epoch=xtrain.shape[0] // batch_n,
             verbose=1,
             callbacks=[lr_reduction, checkpoint],
             validation_data=(xtest, ytest))

# now there are 2 options:
# train another model with best hyperparams
# or
# use one from tuner - tuner.get_best_models()

# get the optimal hyperparameters
best_hps = tuner.get_best_hyperparameters(num_trials=1)[0]


class TerminateOnBaseline(Callback):
    """Callback that terminates training when either acc or val_acc reaches a specified baseline
    """

    def __init__(self, monitor='accuracy', baseline=0.9):
        super(TerminateOnBaseline, self).__init__()
        self.monitor = monitor
        self.baseline = baseline

    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}
        acc = logs.get(self.monitor)
        if acc is not None:
            if acc >= self.baseline:
                print('Epoch %d: Reached baseline, terminating training' % (epoch))
                self.model.stop_training = True


terminate = TerminateOnBaseline(monitor='val_accuracy', baseline=0.875)

# build the model with the optimal hyperparameters and train it on the data
model = tuner.hypermodel.build(best_hps)

history = model.fit(x=xtrain,
                    y=ytrain,
                    batch_size=batch_n,
                    steps_per_epoch=xtrain.shape[0] // batch_n,
                    epochs=245,
                    verbose=1,
                    callbacks=[lr_reduction, terminate],
                    validation_data=(xtest, ytest))

val_acc_per_epoch = history.history['val_accuracy']
best_epoch = val_acc_per_epoch.index(max(val_acc_per_epoch)) + 1
print('Best epoch: %d' % (best_epoch,))

model.save(os.path.join(model_path, model_name) + '.h5')
model = load_model(os.path.join(model_path, model_name) + '.h5')

# # could also train with ideal num of epochs
# # re-instantiate the hypermodel and train it with the optimal number of epochs from above
# hypermodel = tuner.hypermodel.build(best_hps)
#
# hypermodel.fit(x=xtrain,
#                y=ytrain,
#                batch_size=batch_n,
#                steps_per_epoch=xtrain.shape[0] // batch_n,
#                epochs=best_epoch,
#                verbose=1,
#                callbacks=[lr_reduction],
#                validation_data=(xtest, ytest))
#
# # evaluate
# eval_result = hypermodel.evaluate(xtest, ytest)
# print("[test loss, test accuracy]:", eval_result)
