import os
import torch  # also pip install pyaml, torchvision
import cv2

from housekeeping.paths_folders import change_wd, path_getter, maybe_make_dir
from housekeeping.images import list_files, filter_to_jpgs
from models_aux.auxilary import crop_yoloed
from tensorflow.keras.utils import load_img, img_to_array, save_img
# from matplotlib import pyplot as plt

# paths
change_wd('stored_images')
os.getcwd()

# Model
model = torch.hub.load('ultralytics/yolov5', 'yolov5l6')  # or yolov5n - yolov5x6, custom


# predict data
predict_path = path_getter('predict')

# append data
predict_images = list_files(predict_path)  # get images' paths - predict
predict_images = filter_to_jpgs(predict_images)
predict_images_paths = []
for pic in predict_images:
    image_path = os.path.join(predict_path, pic)
    predict_images_paths.append(image_path)


###
# crop all 'predict' images
###

maybe_make_dir(os.path.join(predict_path, 'yoloed'))
end_path = os.path.join(predict_path, 'yoloed')

for img in predict_images_paths:
    yolo_res = model(img)
    yolo_pd = yolo_res.pandas().xyxy[0]
    crop_yoloed(img, yolo_pd, end_path)

###
# process yolo images to run damage detect model later on
###

predict_cropped_path = path_getter('predict_cropped')
yolo_cropped_path = os.path.join(predict_cropped_path, 'yoloed_cropped')
maybe_make_dir(yolo_cropped_path)

predict_yoloed_list = list_files(end_path)
predict_yoloed_list = filter_to_jpgs(predict_yoloed_list)
size = 250  # images to be cropped to...

# resize yoloed images
for pic in predict_yoloed_list:
    sample_img = load_img(os.path.join(end_path, pic))
    sample_img_array = img_to_array(sample_img)

    # now crop the initial image and save it
    init_img = cv2.resize(sample_img_array, (size, size))  # resize
    save_img(os.path.join(yolo_cropped_path, f'init_predict_damaged_{pic}'), init_img)


# # solo test
# img = predict_images_paths[3]  # or file, Path, PIL, OpenCV, numpy, list
#
# # Inference
# results = model(img)
#
# # Results
# results.print()  # or .show(), .save(), .crop(), .pandas(), etc.
# results.show()
#
# pd_res = results.pandas().xyxy[0]
# pd_res.shape[0]
# boxes = pd_res.iloc[:, 0:4]
# boxes.iloc[0]
# classes = pd_res.iloc[:, 6]
# classes[0]
# scores = pd_res.iloc[:, 4]
#
# crops = results.crop(save=True)  # or .show(), .save(), .print(), .pandas(), etc.
#
# single_path = img
# import cv2
# image = cv2.imread(single_path)
# crop_yoloed(single_path, pd_res, end_path)  # test on single
