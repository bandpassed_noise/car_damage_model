import numpy as np
import os
import cv2  # pip install opencv-python

from housekeeping.paths_folders import path_getter, maybe_make_dir, change_wd
from housekeeping.images import list_files, filter_to_jpgs

from keras import optimizers
from keras.models import Sequential
from keras.layers import Dense, Flatten, MaxPooling2D, Dropout, Conv2D

from tensorflow.keras.utils import img_to_array

from keras_tuner.tuners import BayesianOptimization  # pip install keras-tuner

# import warnings
# warnings.filterwarnings("ignore")
model_name = 'model_tuner_base_aug'
project_name = 'tuner_kaggle_toy'
np.set_printoptions(suppress=True)  # scientific notation off


def build_model(hp):
    model_build = Sequential()

    hp_units_1 = hp.Int('units_1', min_value=16, max_value=32, step=16)
    hp_units_2 = hp.Int('units_2', min_value=32, max_value=64, step=32)
    hp_units_3 = hp.Int('units_3', min_value=64, max_value=128, step=64)
    hp_dropout = hp.Choice('rate', values=[0.25, 0.5])
    hp_learning_rate = hp.Choice('learning_rate', values=[0.002, 0.0015])

    model_build.add(Conv2D(hp_units_1, (3, 3), activation='relu', input_shape=(250, 250, 3)))
    model_build.add(Conv2D(hp_units_1, (3, 3), activation='relu', input_shape=(250, 250, 3)))
    model_build.add(MaxPooling2D((2, 2)))
    model_build.add(Conv2D(hp_units_2, (3, 3), activation='relu'))
    model_build.add(MaxPooling2D((2, 2)))
    model_build.add(Conv2D(128, (3, 3), activation='relu'))
    model_build.add(MaxPooling2D((2, 2)))
    model_build.add(Conv2D(128, (3, 3), activation='relu'))
    model_build.add(MaxPooling2D((2, 2)))
    model_build.add(Flatten())
    model_build.add(Dropout(rate=hp_dropout))
    model_build.add(Dense(hp_units_3, activation='relu'))
    model_build.add(Dense(1, activation='sigmoid'))

    adam_opt = optimizers.Adam(learning_rate=hp_learning_rate)

    model_build.compile(loss='binary_crossentropy', optimizer=adam_opt, metrics=['accuracy'])

    return model_build


# paths
change_wd('data')
os.getcwd()

predict_path = path_getter(os.path.join('stored_images', 'predict_cropped', 'yoloed_cropped'))

# model folder
model_path = path_getter(model_name)

tuner = BayesianOptimization(
    build_model,
    objective='val_accuracy',
    max_trials=15,
    directory=model_path,
    project_name=project_name)

# laod n best models and recreate the best one
model = tuner.get_best_models(num_models=3)
model = model[1]

###
# data
###
# predict data
# to populate later on
data_predict = []

# append data
predict_img_paths = list_files(predict_path)  # get images' paths - predict
predict_img_paths = filter_to_jpgs(predict_img_paths)
for pic in predict_img_paths:
    image = cv2.imread(os.path.join(predict_path, pic))
    image = img_to_array(image)
    data_predict.append(image)

data_predict = np.array(data_predict)
data_predict = data_predict / 255.0  # input has to be between 0 and 1

###
# eval & predict
###

print(model.predict(data_predict).shape)
print(predict_img_paths)
print(model.predict(data_predict))

