import os
from housekeeping.paths_folders import change_wd
from models_aux.auxilary import model_gatekeeper

# paths
change_wd('data')
os.getcwd()

folders = list(os.listdir(os.getcwd()))
folders_models = [f for f in folders if 'model' in f]

# delete subpar models
for folder in folders_models:
    iter_models = list(os.listdir(os.path.join(os.getcwd(), folder)))
    iter_models = [m for m in iter_models if 'model' in m]  # only models
    for model in iter_models:
        # print(model)
        model_gatekeeper(folder, model, 875, 875)



